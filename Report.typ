#set text(size: 12pt, font: "Times New Roman")
#show link: underline

#align(center)[
  = BINP28 Project - Mystery Taxa
  #v(1.2em, weak: true)
  #block(text(weight: 100, 1.2em, [Alberto Fabbri]))
]

The aim of this project is to investigate a variant calling file (VCF) containing 16 samples in total, 15 of which, based on their names, seems to be individuals from 3 different populations plus the outgroup individual. The file contains data from two chromosomes, chromosome 5 and chromosome Z.

The first step of this analysis consisted of the filtering of the VCF file to remove low quality data. The file was filtered by depth and missingness. The depth was set to a minimum of 5 and a maximum of 15. This is based on the distribution shown in @depth, the value of 5 is deemed a compromosive given by the low general mean depth (8.44) and the necessity to remove reads that could have been affected by at least one or two sequencing errors. The chosen upper limit is 15 because it does not remove too many reads while being less than two times the mean, thus excluding multiple reads that have been erroneously  mapped to the same area of the genome.

For the filter on missingness, a 5% threshold has been set. This removes all the samples that have some missing data as this is quite rare in the given dataset, as shown in @missingness.

#grid(
  columns: 2, // 2 means 2 auto-sized columns
  gutter: 2mm, // space between columns
  [#figure(
    image("depth.svg", width: 100%),
    caption: [Zoomed in view of the mean depth per site],
  ) <depth>],
  [#figure(
    image("missingness.svg", width: 100%),
    caption: [Zoomed in view of the missingness per site],
  ) <missingness>],
)

To generate the pca data, #link("https://www.cog-genomics.org/plink/")[plink] has been used. Before performing the analysis, linkage pruning has been used to remove weakly predictive variants. @pve shows the variance explained by every principal component (PC) and clearly illustrates that PC1 explains more than a fourth of the total variance while none of the other PCs explain more than 10%.
@pca shows three clusters, the dots have been colored by looking at the names assuming that samples with similar names were from the same population. This clustering is not a proof that that the individuals are from three populations but it is a good indication that further exploration in that direction is needed.


#grid(
  columns: 2, // 2 means 2 auto-sized columns
  gutter: 2mm, // space between columns
  [#figure(
    image("pve.svg", width: 100%),
    caption: [Percentage of variance explained by each principal component],
  ) <pve>],
  [#figure(
    image("pca.svg", width: 100%),
    caption: [Principal component analysis showing three main groups],
  ) <pca>],
)

For the cluster analysis #link("https://dalexander.github.io/admixture/")[ADMIXTURE] has been used. The same analysis has been done varying the `k` parameter (from to 2 to 4 due to hardware limits). This parameter is used to indicate how many ancestral populations the algorithm should assume when estimating ancestry proportions for individuals in the dataset. Furthermore, for every value of `k`, the software return a cross-validation error (CV error) value that can be used to estimate the best clustering.

@k2 is reported as the best clustering with a  CV error of 0.56. According to this estimation the two populations 8N and Lesina have completely different ancestry while K0 has a mixed ancestry that is more similar to 8N.
@k3 has a CV error of 0.70, significantly worse than the `k = 2` plot. This estimates shows the three population as almost completely separated and independent.
This can be an indication that the two of populations 8N and 0K have a common ancestor while the third one, Lesina, is less related to the other two.


#grid(
  columns: 2, // 2 means 2 auto-sized columns
  gutter: 2mm, // space between columns
  [#figure(
    image("k2.svg", width: 100%),
    caption: [Admixture clusters with k = 2]
  ) <k2>],
  [#figure(
    image("k3.svg", width: 100%), 
    caption: [Admixture clusters with k = 3]
  ) <k3>],
),

