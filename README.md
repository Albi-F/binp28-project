# Mystery Taxa

The code for this project can be found on GitLab, on [this repository](https://gitlab.com/Albi-F/binp28-project).

To run this workflow you need both [Conda](https://github.com/conda/conda) and [Snakemake](https://snakemake.github.io/). Install both softwares following the instructions on their respective websites.

From the root folder of this project you can run the following command:

```bash
snakemake --cores 6
```

Adjust the number of cores to your machine.

**N.B.** Snakemake needs an internet connection in order to download the required packages through Conda

The workflow may fail if it consumes all the RAM or if it takes too long for a program to write its output files, if that is the case run Snakemake with the following parameters (adjust them if you need to):

```bash
snakemake --cores 6 --resources mem_mb=14000 --latency-wait 100
```

The folder `workflow` contains the file `Snakefile` with all the steps and programs used for this workflow.
All the generated file are saved inside the `results` folder.

## Plots

To plot the pictures use the R code provided.