## Cleaning up
# Clear the terminal output
# Do this first so that you do not accidentally clear useful error messages
cat("\014")
# Close all open graphical devices
graphics.off()
# Clear existing variables
rm(list = ls())

# load tidyverse package
library(tidyverse)
# fst analysis
library(qqman)
# color the text
library(crayon)
# set working directory programmatically
library(rstudioapi)

# Getting the path of your current open file
current_path = rstudioapi::getActiveDocumentContext()$path
desired_path <- file.path(dirname(current_path))
setwd(dirname(desired_path ))

# Set the color of the headers
color_header <- crayon::green

### FILTERING

## Variant quality
# confidence in the variant calls
cat(color_header("Variant quality\n"))
var_qual <-
  read_delim(
    "results/ProjTaxa.lqual",
    delim = "\t",
    col_names = c("chr", "pos", "qual"),
    skip = 1,
    show_col_types = FALSE
  )
# print(spec(var_qual))
var_qual_plot <-
  ggplot(var_qual, aes(qual)) + geom_density(fill = "dodgerblue1",
                                             colour = "black",
                                             alpha = 0.3) + theme_light() + labs(title = "Density Plot of Variant Quality")
print(var_qual_plot)
print(summary(var_qual$qual))

## Variant mean depth
# number of reads that have mapped to this position
cat(color_header("Variant mean depth\n"))
var_depth <-
  read_delim(
    "results/ProjTaxa.ldepth.mean",
    delim = "\t",
    col_names = c("chr", "pos", "mean_depth", "var_depth"),
    skip = 1,
    show_col_types = FALSE
  )
var_depth_plot <-
  ggplot(var_depth, aes(mean_depth)) + geom_density(fill = "dodgerblue1",
                                                    colour = "black",
                                                    alpha = 0.3) + theme_light() + labs(title = "Density Plot of Variant Mean Depth")
print(var_depth_plot)
print(summary(var_depth$mean_depth))
# plot the interesting range
var_depth_plot_range <- var_depth_plot + xlim(0, 18)
print(var_depth_plot_range)

## Variant missingness
# number of individuals that lack a genotype at a call site
cat(color_header("Variant missingness\n"))
var_miss <-
  read_delim(
    "results/ProjTaxa.lmiss",
    delim = "\t",
    col_names = c("chr", "pos", "nchr", "nfiltered", "nmiss", "fmiss"),
    skip = 1,
    show_col_types = FALSE
  )
var_miss_plot <-
  ggplot(var_miss, aes(fmiss)) + geom_density(fill = "dodgerblue1",
                                              colour = "black",
                                              alpha = 0.3) + theme_light() + labs(title = "Density Plot of Variant Missingness")
print(var_miss_plot)
print(summary(var_miss$fmiss))
# plot the interesting range
var_miss_plot_range <- var_miss_plot + xlim(0, 0.2)
print(var_miss_plot_range)

# Minor allele frequency
## distribution of allele frequencies, used for the minor-allele frequency (MAF) thresholds
cat(color_header("Minor allele frequency\n"))
var_freq <- read_delim(
  "results/ProjTaxa.frq",
  delim = "\t",
  col_names = c("chr", "pos", "nalleles", "nchr", "a1", "a2"),
  skip = 1,
  show_col_types = FALSE
)
# find minor allele frequency
var_freq$maf <-
  var_freq %>% select(a1, a2) %>% apply(1, function(z)
    min(z))
var_freq_plot <-
  ggplot(var_freq, aes(maf)) + geom_density(fill = "dodgerblue1",
                                            colour = "black",
                                            alpha = 0.3) + theme_light() + labs(title = "Minor Allele Frequency")
print(var_freq_plot)
print(summary(var_freq$maf))

## PCA

# Read files
pca <- read_table("results/ProjTaxa.eigenvec", col_names = FALSE, show_col_types = FALSE)
eigenval <- scan("results/ProjTaxa.eigenval")
# sort out the pca data
# remove nuisance column
pca <- pca[,-1]
# set names
names(pca)[1] <- "ind"
names(pca)[2:ncol(pca)] <- paste0("PC", 1:(ncol(pca)-1))
# New column with specific values
new_column <- c("8N", "8N", "8N", "8N", "8N", "K0", "K0", "K0", "K0", "K0", "Lesina", "Lesina", "Lesina", "Lesina", "Lesina")
# Add the new column to the dataframe in the second position
pca <- cbind(pca[1], new_column, pca[-1])
# Rename the new column if necessary
colnames(pca)[2] <- "Group"
# first convert to percentage variance explained
pve <- data.frame(PC = 1:15, pve = eigenval/sum(eigenval)*100)
# make plot
pve_plot <- ggplot(pve, aes(PC, pve)) + geom_bar(stat = "identity") + ylab("Percentage variance explained") + theme_light()
print(pve_plot)
# calculate the cumulative sum of the percentage variance explained
cumsum(pve$pve)
# plot pca
pca_plot <- ggplot(pca, aes(x = PC1, y = PC2, color = Group)) + 
  geom_point(size = 3) +
  scale_colour_manual(values = c("#1f78b4", "#a6cee3", "#b2df8a")) +  # Adjust colors as needed
  coord_equal() +
  theme_light() +
  xlab(paste0("PC1 (", signif(pve$pve[1], 3), "%)")) +
  ylab(paste0("PC2 (", signif(pve$pve[2], 3), "%)"))
print(pca_plot)

## ADMIXTURE
# Names of the samples
names <- c("8N05240", "8N05890", "8N06612", "8N73248", "8N73604", "K006", "K010", "K011", "K015", "K019", "Lesina_280", "Lesina_281", "Lesina_282", "Lesina_285", "Lesina_286")

# K2
adm_k2=read.table("results/ProjTaxa.RAD.2.Q")
adm_k2 <- cbind(names, adm_k2)
adm_k2_long <- pivot_longer(adm_k2, cols = c(V1, V2), names_to = "cluster", values_to = "value")
adm_k2_plot <- ggplot(adm_k2_long, aes(x = names, y = value, fill = cluster)) + 
  geom_bar(stat = "identity", position = "stack") +
  labs(title = "K = 2", x = "Individual", y = "Ancestry", fill = "Clusters") +
  scale_fill_manual(values = c("#b2df8a", "#1f78b4"),  labels = c("Lesina", "8N")) + 
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
print(adm_k2_plot)

# K3
adm_k3=read.table("results/ProjTaxa.RAD.3.Q")
adm_k3 <- cbind(names, adm_k3)
adm_k3_long <- pivot_longer(adm_k3, cols = c(V1, V2, V3), names_to = "cluster", values_to = "value")
adm_k3_plot <- ggplot(adm_k3_long, aes(x = names, y = value, fill = cluster)) + 
  geom_bar(stat = "identity", position = "stack") +
  labs(title = "K = 3", x = "Individual", y = "Ancestry", fill = "Clusters") +
  scale_fill_manual(values = c("#1f78b4", "#b2df8a", "#a6cee3"),  labels = c("8N", "Lesina", "K0")) + 
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1))
print(adm_k3_plot)
