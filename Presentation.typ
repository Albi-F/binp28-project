#import "@preview/polylux:0.3.1": *

#set page(paper: "presentation-16-9")
#set text(size: 15pt)
#set page(margin: 10mm)

#polylux-slide[
#align(horizon + center)[
  #text(size: 50pt)[
    
  = Alberto Fabbri

  Lund University\
  Bioinformatics\
  2023
  ]
]
]


#polylux-slide[
#align(horizon)[
  #text(size: 25pt)[
  = Analysis of unknown samples from a VCF file
  The scope of this project was to analyze unknown samples from a variant calling file.
  The file contained:
  - 16 samples of which one was labelled as the outgroup
  - 3 816 977 variant sites
  - Two chromosomes, 5 and Z
  ]
]
]

#polylux-slide[
#align(horizon + center)[
  = Filtering
#grid(
      columns: 2,     // 2 means 2 auto-sized columns
      gutter: 2mm,    // space between columns
      figure(
        image("depth.svg", width: 100%),
        caption: [Zoomed in view of the mean depth per site]
      ),
      figure(
        image("missingness.svg", width: 100%),
        caption: [Zoomed in view of the missingness per site]
      )
    ),
]
]

#polylux-slide[
#align(horizon + center)[
  = Principal Component Analysis
#grid(
      columns: 2,     // 2 means 2 auto-sized columns
      gutter: 2mm,    // space between columns
      figure(
        image("pve.svg", width: 100%),
        caption: [Percentage of variance explained by each principal component]
      ),
      figure(
        image("pca.svg", width: 100%),
        caption: [Principal component analysis showing three main groups]
      )
    ),
]
]

#polylux-slide[
  #align(horizon + center)[
    = Clusters

    #grid(
      columns: 2,     // 2 means 2 auto-sized columns
      gutter: 2mm,    // space between columns
      figure(
        image("k2.svg", width: 100%),
        caption: [Admixture clusters with k = 2]
      ),
      figure(
        image("k3.svg", width: 100%),
        caption: [Admixture clusters with k = 3]
      )
    ),
  ]
]

#polylux-slide[
  #align(horizon + center)[
    = The real population

    #figure(
      image("sparrows.jpeg", width: 50%),
      caption: [Admixture clusters with k = 2]
    )
  ]
]